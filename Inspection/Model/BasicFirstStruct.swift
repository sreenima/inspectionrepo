//
//  BasicFirstStruct.swift
//  Inspection
//
//  Created by Anargha  on 07/05/20.
//  Copyright © 2020 CodeGreen. All rights reserved.
//

import UIKit

struct BasicFirstStruct : Equatable {
    static func ==(lhs: BasicFirstStruct, rhs: BasicFirstStruct) -> Bool {
        return lhs.PONo == rhs.PONo
    }
    
    var fabricCategory : String = "Group 1"
    var PONo : String = ""
    var content : String = ""
    var construction : String = ""
    var POCutWidth : Float = 0.0
    var factoryName : String = ""
    var fabricType : String = "Woven"
    var orderQty : Int = 0
    var totalQtyOffered : Int = 0
    var weightGSM : Float = 0.0
    var colorName : String = ""
    var finish : String = ""
    var reportToName : String = ""
    var date : String = ""
    var inspectionNo : Int = 0
    var id  : Int = 0
    
    init(dict:[String : Any], isApi : Bool = false) {
        if !isApi {
            if let fabricCategory = dict["fabricCategory"] as? String {
                self.fabricCategory = fabricCategory
            }
            if let PONo = dict["poNo"] as? String {
                self.PONo = PONo
            }
            if let content = dict["content"] as? String {
                self.content = content
            }
            if let construction = dict["construction"] as? String {
                self.construction = construction
            }
            if let POCutWidth = dict["poCutWidth"] as? Float {
                self.POCutWidth = POCutWidth
            }
            if let factoryName = dict["factoryName"] as? String {
                self.factoryName = factoryName
            }
            if let fabricType = dict["fabricType"] as? String {
                self.fabricType = fabricType
            }
            if let orderQty = dict["orderQty"] as? Int {
                self.orderQty = orderQty
            }
            if let totalQtyOffered = dict["totalQtyOffered"] as? Int {
                self.totalQtyOffered = totalQtyOffered
            }
            if let weightGSM = dict["weightGSM"] as? Float {
                self.weightGSM = weightGSM
            }
            if let colorName = dict["colorName"] as? String {
                self.colorName = colorName
            }
            if let finish = dict["finish"] as? String {
                self.finish = finish
            }
            if let reportToName = dict["reportToName"] as? String {
                self.reportToName = reportToName
            }
            if let inspectionNo = dict["inspectionNo"] as? Int {
                self.inspectionNo = inspectionNo
            }
            if let date = dict["date"] as? String {
                self.date = date
            }
        } else {
            if let poNumber = dict["po_number"] as? String {
                self.PONo = poNumber
            }
            if let id = dict["id"] as? Int {
                self.id = id
            }
            if let date = dict["added_on"] as? String {
                self.date = date
            }
        }
    }
}

struct BasicSecondStruct {
    var rollNumber : String = ""
    var ticketLength : Float = 0.0
    var actualLength : Float = 0.0
    var actualCutWidthOne : Float = 0.0
    var actualCutWidthTwo : Float = 0.0
    var actualCutWidthThree : Float = 0.0
    var endToEnd : String = ""
    var sideToSide : String = ""
    var sideToCenter : String = ""
    var skewBowing : String = ""
    var pattern : Bool = true
    var actualWeightGSM : Float = 0.0
    var handFeel : Bool = false
    var inspectionNo : Int = 0
    
    init(dict : [String : Any]) {
        if let rollNumber = dict["rollNumber"] as? String {
            self.rollNumber = rollNumber
        }
        if let ticketLength = dict["ticketLength"] as? Float {
            self.ticketLength = ticketLength
        }
        if let actualLength = dict["actualLength"] as? Float {
            self.actualLength = actualLength
        }
        if let actualCutWidthOne = dict["actualCutWidthOne"] as? Float {
            self.actualCutWidthOne = actualCutWidthOne
        }
        if let actualCutWidthTwo = dict["actualCutWidthTwo"] as? Float {
            self.actualCutWidthTwo = actualCutWidthTwo
        }
        if let actualCutWidthThree = dict["actualCutWidthThree"] as? Float {
            self.actualCutWidthThree = actualCutWidthThree
        }
        if let endToEnd = dict["endToEnd"] as? String {
            self.endToEnd = endToEnd
        }
        if let sideToSide = dict["sideToSide"] as? String {
            self.sideToSide = sideToSide
        }
        if let sideToCenter = dict["sideToCenter"] as? String {
            self.sideToCenter = sideToCenter
        }
        if let skewBowing = dict["skewBowing"] as? String {
            self.skewBowing = skewBowing
        }
        if let pattern = dict["pattern"] as? Bool {
            self.pattern = pattern
        }
        if let actualWeightGSM = dict["actualWeightGSM"] as? Float {
            self.actualWeightGSM = actualWeightGSM
        }
        if let handFeel = dict["handFeel"] as? Bool {
            self.handFeel = handFeel
        }
        if let inspectionNo = dict["inspectionNo"] as? Int {
            self.inspectionNo = inspectionNo
        }
    }
    
}

struct GeneralStruct {
    var id : Int
    var title : String
    
    init(dict : [String : Any]) {
        self.id = dict["id"] as! Int
        self.title = dict["title"] as! String
    }
}
