//
//  SummaryStruct.swift
//  Inspection
//
//  Created by Anargha  on 18/05/20.
//  Copyright © 2020 CodeGreen. All rights reserved.
//

import Foundation


struct SummaryFirstStruct {
    var rollNumber : String = ""
    var shrinkage : Double = 0.0
    var torque : Double = 0.0
    
    init(dict : [String : Any]) {
        if let rollNumber = dict["rollNumber"] as? String {
            self.rollNumber = rollNumber
        }
        if let shrinkage = dict["shrinkage"] as? Double {
            self.shrinkage = shrinkage
        }
        if let torque = dict["torque"] as? Double {
            self.torque = torque
        }
    }
}

struct SummaryDataStruct {
    var isAccepted : Bool
    var comments : String
    var inspectionNo : Int
    var acceptedRolls : Int
    var rejectedRolls : Int
    var avgPoints : Int
    
    init(status : Bool , comm : String, inspectionNo : Int, accepRolls : Int, rejRolls : Int,avgPoints : Int) {
        self.isAccepted = status
        self.comments = comm
        self.inspectionNo = inspectionNo
        self.acceptedRolls = accepRolls
        self.rejectedRolls = rejRolls
        self.avgPoints = avgPoints
    }
}
